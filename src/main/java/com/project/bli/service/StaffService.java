package com.project.bli.service;

import java.util.List;
import java.util.Optional;

import com.project.bli.entity.Staff;

public interface StaffService {
	
	public List<Staff> getStaffs();
	public Optional<Staff> getStaffDetailByNip(String nip);
	public boolean addStaff(Staff staff);
	public boolean updateStaff(Staff staff);
	public boolean deleteStaff(String nip);
	public boolean isStaffExists(String nip);

}
