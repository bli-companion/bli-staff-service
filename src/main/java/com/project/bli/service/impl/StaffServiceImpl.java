package com.project.bli.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.project.bli.entity.Division;
import com.project.bli.entity.Staff;
import com.project.bli.model.SignUpRequest;
import com.project.bli.repository.DivisionRepository;
import com.project.bli.repository.StaffRepository;
import com.project.bli.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StaffServiceImpl.class);
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private StaffRepository staffRepository;
	
	@Autowired
	private DivisionRepository divisionRepository;
	
	@Override
	public List<Staff> getStaffs() {
		return staffRepository.findAll();
	}

	@Override
	public Optional<Staff> getStaffDetailByNip(String nip) {
		return staffRepository.findById(nip);
	}

	@Override
	public boolean addStaff(Staff staff) {
		LOGGER.info("Add staff service triggered!");
		System.out.println("Received Staff: " + staff);
		
		// Check if NIP is available
		if (isStaffExists(staff.getNip())) return false;
		
		// Check if division ID exists
		Optional<Division> findDivision = divisionRepository.findById(staff.getDivision().getId());
		if (!findDivision.isPresent()) {
			LOGGER.error("Division does not exists ...");
			return false;
		}
		
		LOGGER.info("Setting neccessary details for staff ...");
		Division division = findDivision.get();
		// Set related object with Staff
		staff.setDomain("U" + staff.getNip());
		staff.setDivision(division);
		staff.setCreated_at(new Date());
		staff.setCreated_by(staff.getCreated_by());
		
		LOGGER.info("Attempting to save staff ...");
		staffRepository.save(staff);
		
		LOGGER.info("Generating credentials ...");
		SignUpRequest signUpRequest = new SignUpRequest();
		signUpRequest.setUsername(staff.getNip());
		signUpRequest.setPassword(staff.getNip());
		signUpRequest.setRole(staff.getRole().name());
		System.out.println("Generated Credential: " + signUpRequest);
		
		boolean flag = addCredential(signUpRequest);
		
		if (!flag) {
			LOGGER.info("Failed to add new credential ...");
			return false;
		}
		
		LOGGER.info("Add staff potentially successful ...");
		return true;
	}

	@Override
	public boolean updateStaff(Staff staff) {
		LOGGER.info("Update staff service triggered!");
		
		// Check if staff with NIP exists		
		if (!isStaffExists(staff.getNip())) return false;
		
		// Check if division ID exists
		LOGGER.info("Check if division exists ...");
		Optional<Division> findDivision = divisionRepository.findById(staff.getDivision().getId());
		if (!findDivision.isPresent()) {
			LOGGER.error("Division does not exists ...");
			return false;
		}
				
		Division division = findDivision.get();
		
		// Set related object with Staff
		staff.setDivision(division);
		staff.setUpdated_at(new Date());
		staff.setUpdated_by(staff.getUpdated_by());
		
		staffRepository.save(staff);
		LOGGER.info("Update staff potentially successful ...");
		return true;
	}

	@Override
	public boolean deleteStaff(String nip) {
		LOGGER.info("Detele staff service triggered!");
		
		if (!isStaffExists(nip)) return false;
		Staff staff = staffRepository.findById(nip).get();
		
		staff.setIs_deleted(true);
		staff.setUpdated_at(new Date());
		
		staffRepository.save(staff);
		LOGGER.info("Delete staff potentially successful ...");
		return true;
	}
	
	/**
	 * Check if NIP is available
	 * 
	 * Param Integer nip
	 * Return boolean
	 */
	@Override
	public boolean isStaffExists(String nip) {
		LOGGER.info("Checking NIP existance ...");
		if (staffRepository.existsById(nip)) {
			LOGGER.info("NIP exists ...");
			return true;
		} else {
			LOGGER.error("Staff with NIP: " + nip + " does not exists ...");
			return false;
		}
	}
	
	public boolean addCredential(SignUpRequest signUpRequest) {
		LOGGER.info("Attempting to add new staff credential ...");
		String url = "http://bli-auth-service/signup";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<SignUpRequest> entity = new HttpEntity<SignUpRequest>(signUpRequest, headers);
		
		try {
			ResponseEntity<SignUpRequest> responseEntity = 
				restTemplate.exchange(
					url,
					HttpMethod.POST, 
					entity, 
					SignUpRequest.class);
			System.out.println("Error status: " + responseEntity.getStatusCode());
			if (responseEntity.getStatusCode().equals(HttpStatus.CREATED)) {
				LOGGER.info("Potential success ...");
				return true;
			}
			LOGGER.info("Potentially fail ...");
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("OUTSIDE FAIL ...");
		return false;
	}

}
