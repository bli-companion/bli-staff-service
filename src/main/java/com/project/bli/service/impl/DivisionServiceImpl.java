package com.project.bli.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.bli.entity.Division;
import com.project.bli.repository.DivisionRepository;
import com.project.bli.service.DivisionService;

@Service
public class DivisionServiceImpl implements DivisionService {
	
	protected final static Logger LOGGER = LoggerFactory.getLogger(DivisionServiceImpl.class);

	@Autowired
	private DivisionRepository divisionRepository;
	
	@Override
	public List<Division> getDivisions() {
		return divisionRepository.findAll();
	}

	@Override
	public Optional<Division> getDivisionById(Integer id) {
		return divisionRepository.findById(id);
	}

	@Override
	public boolean addDivision(Division division) {
		LOGGER.info("Add division service triggered!");
		if (isDivisionExists(division.getId())) {
			LOGGER.info("Division with ID: " + division.getId() + " already exists ...");
			return false;
		}
		divisionRepository.save(division);
		LOGGER.info("Adding new division potentially successful ...");
		return true;
	}

	@Override
	public boolean updateDivision(Division division) {
		LOGGER.info("Update division service triggered!");
		
		// Check if division exists
		if (!isDivisionExists(division.getId())) return false;
		
		LOGGER.info("Updating division potentially successful ...");
		divisionRepository.save(division);
		return true;
		
	}

	@Override
	public boolean deleteDivision(Integer id) {
		LOGGER.info("Delete division service triggered!");
		
		// Check if division exists
		if (!isDivisionExists(id)) return false;
		
		divisionRepository.deleteById(id);
		LOGGER.info("Deleting division potentially successful ...");
		return true;
	}

	@Override
	public boolean isDivisionExists(Integer id) {
		LOGGER.info("Checking division existence by ID ...");
		if (divisionRepository.existsById(id)) {
			LOGGER.info("Division exists ...");
			return true;
		} else {
			LOGGER.info("Division does not exist ...");
			return false;
		}		
	}

}
