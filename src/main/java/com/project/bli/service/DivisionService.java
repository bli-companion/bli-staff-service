package com.project.bli.service;

import java.util.List;
import java.util.Optional;

import com.project.bli.entity.Division;

public interface DivisionService {

	List<Division> getDivisions();
	Optional<Division> getDivisionById(Integer id);
	boolean addDivision(Division division);
	boolean updateDivision(Division division);
	boolean deleteDivision(Integer id);
	boolean isDivisionExists(Integer id);
	
}
