package com.project.bli.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, String> {

}
