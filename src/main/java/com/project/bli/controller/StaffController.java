package com.project.bli.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.bli.entity.Staff;
import com.project.bli.service.impl.StaffServiceImpl;

@RestController
//@RequestMapping("/api/staff")
public class StaffController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StaffController.class);

	@Autowired
	private StaffServiceImpl staffServiceImpl;
	
	@GetMapping("/all")
	public List<Staff> getStaffs() {
		LOGGER.info("Retrieving all staffs ...");
		return staffServiceImpl.getStaffs();
	}
	
	@GetMapping("/detail/{nip}")
	public ResponseEntity<Optional<Staff>> getStaffById(@PathVariable String nip) {
		LOGGER.info("Initializing staff detail with NIP: " + nip + " ...");
		Optional<Staff> staff = staffServiceImpl.getStaffDetailByNip(nip);
		return new ResponseEntity<Optional<Staff>>(staff, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> addStaff(@RequestBody Staff staff) {
		LOGGER.info("Initializing adding new staff ...");
		boolean flag = staffServiceImpl.addStaff(staff);
		
		if (flag) {
			LOGGER.info("New staff created successfully ...");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} else {
			LOGGER.info("Failed to create new staff ...");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@PutMapping
	public ResponseEntity<Void> updateStaff(@RequestBody Staff staff) {
		LOGGER.info("Initializing updating staff with NIP: " + staff.getNip() + " ...");
		boolean flag = staffServiceImpl.updateStaff(staff);
		
		if (flag) {
			LOGGER.info("Staff with NIP: " + staff.getNip() + " updated successfully ...");
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			LOGGER.info("Failed to update new staff ...");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@DeleteMapping("/delete/{nip}")
	public ResponseEntity<Void> deleteStaff(@PathVariable String nip) {
		LOGGER.info("Initializing delete staff with NIP: " + nip + "...");
		boolean flag = staffServiceImpl.deleteStaff(nip);
		if (flag) {
			LOGGER.info("Staff with NIP: " + nip + "deleted successfully ...");
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
		LOGGER.info("Failed to delete staff with NIP: " + nip + " ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
}
