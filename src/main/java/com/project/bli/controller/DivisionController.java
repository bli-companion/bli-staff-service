package com.project.bli.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.bli.entity.Division;
import com.project.bli.service.impl.DivisionServiceImpl;

@RestController
@RequestMapping("/division")
public class DivisionController {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(DivisionController.class);

	@Autowired
	private DivisionServiceImpl divisionServiceImpl;
	
	@GetMapping
	public ResponseEntity<List<Division>> getDivisions() {
		LOGGER.info("Initializing get all list of division(s) ...");
		List<Division> divisionList = divisionServiceImpl.getDivisions();
		return new ResponseEntity<List<Division>>(divisionList, HttpStatus.OK);
	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<Optional<Division>> getDivisionById(@PathVariable Integer id) {
		LOGGER.info("Retrieving division with  ID: " + id + " ...");
		Optional<Division> division = divisionServiceImpl.getDivisionById(id);
		return new ResponseEntity<Optional<Division>>(division, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> addDivision(@RequestBody Division division) {
		LOGGER.info("Attempting to insert new division to database ...");
		boolean flag = divisionServiceImpl.addDivision(division);
		
		if (flag) {
			LOGGER.info("New division successfully created ...");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		LOGGER.info("Adding division issue / already registered");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@PutMapping
	public ResponseEntity<Void> updateDivision(@RequestBody Division division) {
		LOGGER.info("Attempting to update existing division ...");
		boolean flag = divisionServiceImpl.updateDivision(division);
		if (flag) {
			LOGGER.info("Successfully updating existing division ...");
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		LOGGER.info("Failed to update division with ID: " + division.getId() + " ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteDivision(@PathVariable Integer id) {
		LOGGER.info("Attempting to delete division with ID: " + id + " ...");
		boolean flag = divisionServiceImpl.deleteDivision(id);
		if (flag) {
			LOGGER.info("Delete successful ....");
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
		LOGGER.info("Failed to delete division with ID: " + id + " ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
}
