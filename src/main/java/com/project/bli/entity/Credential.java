package com.project.bli.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

public class Credential implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CREDENTIAL_SEQ")
	@SequenceGenerator(sequenceName="credential_seq", allocationSize=1, name="CREDENTIAL_SEQ")
	private Integer id;
	
	@Nullable
	@Size(max = 100)
	private String password;
	
	@NotNull
	private Date last_active;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@Column(nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@NotNull
	private String created_by;
	
	@NotNull
	private String updated_by;
	
	@Column(name="is_deleted")
	private boolean is_deleted = false;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "nip", nullable = false)
	private Staff staff;
	
	public Credential() {
		
	}
	
	public Credential(String domain, String password, Date lastActive, Date createdAt,
			Date updatedAt, String createdBy, String updatedBy, boolean isDeleted) {
		this.password = password;
		this.last_active = lastActive;
		this.created_at = createdAt;
		this.updated_at = updatedAt;
		this.created_by = createdBy;
		this.updated_by = updatedBy;
		this.is_deleted = isDeleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLast_active() {
		return last_active;
	}

	public void setLast_active(Date last_active) {
		this.last_active = last_active;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	@Override
	public String toString() {
		return "Credential [id=" + id + ", password=" + password + ", last_active=" + last_active + ", created_at="
				+ created_at + ", updated_at=" + updated_at + ", created_by=" + created_by + ", updated_by="
				+ updated_by + ", is_deleted=" + is_deleted + ", staff=" + staff + "]";
	}
	
}
